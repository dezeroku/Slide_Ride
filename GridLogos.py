from Getch import getch
import sys
import os


def clear_screen():
    if os.name == 'posix':
        # clear
        os.system("clear")
        print("clear")
    elif os.name == 'nt':
        os.system("cls")
        print("cls")


def logos(number):

    clear_screen()
    if number == 1:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#  XXXXX X             XXX     XXXXXXXXX        XXXXXXXX           #")
        print("#  X     X             XXX     XX       X       XX                 #")
        print("#  XXXXX X             XXX     XX        X      XX                 #")
        print("#      X X             XXX     Xx         X     XX                 #")
        print("#  XXXXX XXXXX         XXX     Xx          X    XX                 #")
        print("#                      XXX     XX         X     XXXXXXXX           #")
        print("#        XXXX          XXX     XX        X      XX                 #")
        print("#        X  X          XXX     XX       X       XX                 #")
        print("#        XXXX          XXX     XX      X        XX                 #")
        print("#        X   X         XXX     XXXXXXXX         XXXXXXXX           #")
        print("#        X    X                                                    #")
        print("#                                                                  #")
        print("#     Push any button to play ( h for help)(o for options)         #")
        print("####################################################################")
        temp = getch()
        if temp == "h":
            logos(2)

    elif number == 2:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#       SLIDE                                    JUMP              #")
        print("#                                                                  #")
        print("#   ##########                             ################        #")
        print("#   #   S    #                             #    SPACE     #        #")
        print("#   ##########                             ################        #")
        print("#        S                                                         #")
        print("#       down                                                       #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#    X =this is you                                                #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                         Push any button to play                  #")
        print("####################################################################")
        getch()

    elif number == 3:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#     X   X   XXX   X   X         x       x   XXX   x   x          #")
        print("#      X X   x   x  X   x         x   x   x  x   x  XX  x          #")
        print("#       X    X   X  X   X         x   x   x  x   x  x x x          #")
        print("#       X    x   x  x   x         x   x   x  x   x  x  XX          #")
        print("#       x     XXX    XXX           XXX XXX    xxx   x   x          #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                        I tak umrzesz 3:)                         #")
        print("#                      You'll die anyway 3:)                       #")
        print("#                                                                  #")
        print("#           Push any button to keep playing (q to quit)            #")
        print("####################################################################")
        temp = getch()
        if temp == q:
            sys.exit()

    elif number == 4:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#                                                                  #")
        print("#     X   X   XXX   X   X         x       x   XXX   x   x          #")
        print("#      X X   x   x  X   x         x   x   x  x   x  XX  x          #")
        print("#       X    X   X  X   X         x   x   x  x   x  x x x          #")
        print("#       X    x   x  x   x         x   x   x  x   x  x  XX          #")
        print("#       x     XXX    XXX           XXX XXX    xxx   x   x          #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                 I tak umrzesz 3:)  (kurde, jednak nie...)        #")
        print("#               You'll die anyway 3:) (shieet, maybe not...)       #")
        print("#                                                                  #")
        print("#   Push r to restart the game          (q to quit)(o to rank)     #")
        print("####################################################################")
        temp = getch()
        while temp != "r":
            if temp == "q":
                sys.exit()
            if temp == "o":
                return None
                # OPEN RANKING
            temp = getch()

    elif number == 5:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#                                                                  #")
        print("#    X   X   XXX   X   X       X        XXX     XXXX    XXXXX      #")
        print("#     X X   X   X  X   X       X       X   X   x          x        #")
        print("#      X    X   X  X   X       X       X   X    XXXX      x        #")
        print("#      X    x   x  x   x       x       X   x        x     x        #")
        print("#      x     xxx    xxx        xxxxx    xxx     XXXX      x        #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                        I tak umrzesz 3:)  (kurde, jednak nie...) #")
        print("#                      You'll die anyway 3:) (shieet, maybe not...)#")
        print("#                                                                  #")
        print("#   Push r to restart the game          (q to quit)(o to rank)     #")
        print("####################################################################")
        temp = getch()
        while temp != "r":
            if temp == "q":
                sys.exit()
            if temp == "o":
                return None
                # OPEN RANKING
            temp = getch()

    elif number == 6:
        print("####################################################################")
        print("#                                                              d0ku#")
        print("#                                                                  #")
        print("#                  XXX      X       X   X   XXXX    XXXX           #")
        print("#                 X   X    X X      X   X  X       X               #")
        print("#                 XXXX    XXXXX     X   X  XXXXX   XXXXX           #")
        print("#                 X      X     X    X   X      X   X               #")
        print("#                 X     X       X   XXXXX  XXXX     XXXX           #")
        print("#                                                                  #")
        print("#                                                                  #")
        print("#                   Push any button to continue                    #")
        print("#                      (h) for help                                #")
        print("#                      (q) to exit                                 #")
        print("#                                                                  #")
        print("####################################################################")
        temp = getch()
        if temp == "q":
            sys.exit()
        elif temp == "h":
            logos(2)
